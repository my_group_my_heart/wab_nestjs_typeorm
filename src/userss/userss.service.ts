import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserssDto } from './dto/create-userss.dto';
import { UpdateUserssDto } from './dto/update-userss.dto';
import { Userss } from './entities/userss.entity';

@Injectable()
export class UserssService {
  constructor(
    @InjectRepository(Userss)
    private usersRepository: Repository<Userss>,
  ) {}

  create(createUserssDto: CreateUserssDto): Promise<Userss> {
    const user: Userss = new Userss();
    user.login = createUserssDto.login;
    user.name = createUserssDto.name;
    user.password = createUserssDto.password;
    return this.usersRepository.save(user);
  }

  findAll(): Promise<Userss[]> {
    return this.usersRepository.find();
  }

  findOne(id: number): Promise<Userss> {
    return this.usersRepository.findOneBy({ id: id });
  }

  async update(id: number, updateUserssDto: UpdateUserssDto) {
    const user = await this.usersRepository.findOneBy({ id: id });
    const updatedUser = { ...user, ...updateUserssDto };

    return this.usersRepository.save(updatedUser);
  }

  async remove(id: number) {
    const user = await this.usersRepository.findOneBy({ id: id });
    return `This action removes a #${id} userss`;
  }
}

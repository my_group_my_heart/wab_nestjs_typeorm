import { Module } from '@nestjs/common';
import { UserssService } from './userss.service';
import { UserssController } from './userss.controller';
import { TypeOrmModule } from '@nestjs/typeorm/dist';
import { Userss } from './entities/userss.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Userss])],
  controllers: [UserssController],
  providers: [UserssService],
})
export class UserssModule {}

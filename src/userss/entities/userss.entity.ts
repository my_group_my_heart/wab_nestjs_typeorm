import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Userss {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ unique: true })
  login: string;
  @Column()
  name: string;
  @Column()
  password: string;
}
